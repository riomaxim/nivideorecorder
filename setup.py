from setuptools import setup

setup(name='nivideorecorder',
      version='0.1',
      description='Simple visualizer recorder for NI card and cameras',
      url='http://TODO',
      author='Maxime RIO',
      author_email='maxime.rio@ucl.ac.uk',
      license='MIT',
      packages=['nivideorecorder'],
      install_requires=[
          'nidaqmx'
      ],
      zip_safe=False)
