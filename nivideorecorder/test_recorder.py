import time
from multiprocessing import Array, Process
from contextlib import contextmanager
from collections import deque

import numpy as np
import matplotlib.pyplot as plt
import nidaqmx
from nidaqmx import constants, stream_readers


def ni_acquire_data(channels, rate, samps_per_chan, data_buffer):
    # TODO add filename input and write data to file
    # TODO retrieve timestamp and diagnose speed
    # TODO replace print statement with logging

    with nidaqmx.Task() as task:
        for chan_kwargs in channels:
            task.ai_channels.add_ai_voltage_chan(**chan_kwargs)

        task.timing.cfg_samp_clk_timing(
            rate=rate,
            sample_mode=constants.AcquisitionType.CONTINUOUS,
            samps_per_chan=samps_per_chan,
        )

        # task.in_stream.input_buf_size = samples_per_buffer * 10  # plus some extra space
        reader = stream_readers.AnalogMultiChannelReader(task.in_stream)

        def reading_task_callback(
            task_idx, event_type, num_samples, callback_data=None
        ):
            with data_buffer.get_array() as arr:
                reader.read_many_sample(
                    arr, num_samples, timeout=constants.WAIT_INFINITELY
                )
            return 0

        task.register_every_n_samples_acquired_into_buffer_event(
            samps_per_chan, reading_task_callback
        )
        task.start()
        print("started")

        # task.wait_until_done(constants.WAIT_INFINITELY)
        while True:
            time.sleep(60)


class LockedArray:
    def __init__(self, shape, dtype):
        self.shape = shape
        c_type = np.ctypeslib.as_ctypes_type(dtype)
        self.shared_array = Array(c_type, int(np.prod(shape)))

    @contextmanager
    def get_array(self):
        with self.shared_array.get_lock():
            shared_buffer = self.shared_array.get_obj()
            yield np.frombuffer(shared_buffer).reshape(self.shape)


rate = 1000
samps_per_chan = 100
channels = [
    {"physical_channel": "Dev1/ai5"},
    {"physical_channel": "Dev1/ai7"},
    {
        "physical_channel": "Dev1/ai8",
        "min_val": -5,
        "max_val": 5,
        "terminal_config": constants.TerminalConfiguration.RSE,
    },
]

data_buffer = LockedArray((len(channels), samps_per_chan), np.float64)
read_proc = Process(
    target=ni_acquire_data, args=(channels, rate, samps_per_chan, data_buffer)
)
read_proc.start()

fig, ax = plt.subplots()
values = deque(maxlen=200)
while True:
    with data_buffer.get_array() as arr:
        value = arr.mean(axis=-1)
    values.append(value)
    ax.clear()
    ax.plot(values)
    plt.pause(0.1)

read_proc.terminate()
read_proc.join()
